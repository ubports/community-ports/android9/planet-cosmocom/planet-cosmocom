#!/bin/bash
set -xe

# Cosmo Communicator specific hack, unpack deb packages needed for CoDi daemon to overlay
[ -f python3-pydbus_0.6.0-2build1_all.deb ] || wget http://archive.ubuntu.com/ubuntu/pool/universe/p/pydbus/python3-pydbus_0.6.0-2build1_all.deb
[ -f python3-serial_3.4-5.1_all.deb ] || wget http://archive.ubuntu.com/ubuntu/pool/main/p/pyserial/python3-serial_3.4-5.1_all.deb
[ -f python-evdev-doc_1.3.0+dfsg-1build1_arm64.deb ] || wget http://ports.ubuntu.com/ubuntu-ports/pool/universe/p/python-evdev/python-evdev-doc_1.3.0+dfsg-1build1_arm64.deb
dpkg-deb -x python3-pydbus_0.6.0-2build1_all.deb "overlay/system"
dpkg-deb -x python3-serial_3.4-5.1_all.deb "overlay/system"
dpkg-deb -x python-evdev-doc_1.3.0+dfsg-1build1_arm64.deb "overlay/system"
mkdir -p "overlay/system/usr/lib"
cp -rv codi-app/codi-app "overlay/system/usr/lib/codi"
